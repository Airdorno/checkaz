//
// Created by theo on 25.03.16.
//

#ifndef CHECKAZ_MOVE_H
#define CHECKAZ_MOVE_H

#include <utility>
#include <vector>
#include "Checkers.h"

using namespace std;

class Move{

public:

    vector<pair<int, int>> positions;

    Move(vector<pair<int, int>> pos){positions = pos;};

    Move();

    string str(bool isBlack){

        ostringstream os;

        for(unsigned i = 0; i < positions.size(); i++){


            //printf("Field: (%i, %i): %i\n", positions.at(i).first, positions.at(i).second,  (4 - positions.at(i).first) + positions.at(i).second * 4 + 1);

            int pos = (4 - positions.at(i).first) + positions.at(i).second * 4;

            if(!isBlack){pos = 33 - pos;}

            os << pos;

            if(positions.size() - i > 1){

                if(abs(positions.at(i + 1).second - positions.at(i).second) == 2){
                    os << "x";
                }else{os << "-";};
                }
        }

        os << "\n";

        return os.str();



    };

    int numberOfJumps(bool isBlack){
        string s = str(isBlack);
        int counter = 0;


        for(unsigned i = 0; i < s.size(); i++){

            if(s.at(i) == 'x'){
                counter++;
            }

        }

        return counter;
    }



};

#endif //CHECKAZ_MOVE_H
