#include <string.h>
#include <iostream>
#include <unistd.h>
#include "Checkers.h"


const int FD_IN  = 3;
const int FD_OUT = 4;

const int BUFFERSIZE_IN  = 35;
const int BUFFERSIZE_OUT = 39;

const int BUFFERSIZE = max(FD_IN, FD_OUT);

template <typename... ARGS>
static void error (const char * fmt, const ARGS& ... arg)
{
    fprintf (stderr, fmt, arg...);
    exit (-1);
}

void input (char* buffer)
{
    char* end = buffer + BUFFERSIZE_IN;
    while (buffer < end) {
        int bytes_read = read (FD_IN, buffer, end - buffer);
        if (bytes_read < 0) error ("error reading FD_IN\n");
        buffer += bytes_read;
    }
    end--;
    if (*end != '\n') error ("line does not end with newline\n");
    *end = 0;
}
void output (char* buffer) {

    int l = strlen(buffer);
    if (buffer[l - 1] != '\n') {
        std::cerr << "too long, had to cut." << std::endl;
        buffer[l - 1] = '\n';
    }
    if (write(FD_OUT, buffer, l) != l) error("error writing FD_OUT\n");
}

int main()
{

    char buffer[BUFFERSIZE];

    input(buffer);

    Checkers c(buffer);

    char temp[100];

    strcpy(temp, c.nextMove().c_str());

    output(temp);

    while (1) {

        input(buffer);

        strcpy(temp, c.nextMove().c_str());

        printf("test \n");

        output(temp);

        // program complete, enter when ready
        //
        // TODO write your own player here, have a look at example_player.cc
        // TODO input() and output() to understand the protocol between
        // TODO MCP and your player (receive game state, send move back)
    }
}


