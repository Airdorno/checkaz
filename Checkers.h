//
// Created by theo on 25.03.16.
//

#ifndef CHECKAZ_CHECKERS_H
#define CHECKAZ_CHECKERS_H
#include <vector>
#include <sstream>      // osstream
#include <utility>      // pair
#include "Move.h"

using namespace std;

const int COLLUMNS = 4;
const int ROWS = 8;

class Checkers{

    char* buffer;

public:

    int field[COLLUMNS][ROWS];

    char color;


    bool isBlack(){return color == 'B';};

    void setupField() {

        color = buffer[0];
        char enemy;

        switch (color) {
            case 'B':
                enemy = 'W';
                break;
            case 'W':
                enemy = 'B';
                break;
            default:
                printf("false input");
                exit(-1);
        }

        char position;

        for (int row = 0; row < ROWS; row++) {
            for (int collumn = 0; collumn < COLLUMNS; collumn++) {
                if (color == 'B') {position = buffer[row * COLLUMNS + collumn + 2];}
                else {position = buffer[33 - (row * COLLUMNS + collumn)];}

                //printf("%c", position);

                if (position == '-') {
                    field[3 - collumn][row] = 0;
                }
                else if (position == tolower(color)) {
                    field[3 - collumn][row] = 1;
                }
                else if (position == toupper(color)) {
                    field[collumn][row] = 2;
                } else if (position == tolower(enemy)) {
                    field[3 - collumn][row] = 3;
                }
                else if (position == toupper(enemy)) {
                    field[3 - collumn][row] = 4;
                } else {
                    field[3 - collumn][row] = -1;
                    printf("error reading in the field");
                }
                //printf("%i", field[collumn][row]);
            }
            //printf("\n");
        }
    };

    Checkers(char* input){buffer = input;};

    pair<int, int> getForwardLeft(pair<int, int> pos){
        return pair<int, int>(pos.first - 1 + pos.second % 2, pos.second + 1);
    };
    pair<int, int> getForwardRight(pair<int, int> pos){
        return pair<int, int>(pos.first + pos.second % 2, pos.second + 1);
    };
    pair<int, int> getBackwardLeft(pair<int, int> pos){
        return pair<int, int>(pos.first - 1  + pos.second % 2, pos.second - 1);
    };
    pair<int, int> getBackwardRight(pair<int, int> pos){
        return pair<int, int>(pos.first + pos.second % 2, pos.second - 1);
    };

    int getField(int collumn, int row) {

        if(collumn >= COLLUMNS || row >= ROWS || collumn < 0 || row < 0){
            return -1;
        }
        else return field[collumn][row];
    };
    int getField(pair<int, int> pos){
        return getField(pos.first, pos.second);
    }

    string getMove(int cA, int rA, int cB, int rB){

        //printf("Move: (%i, %i) -> (%i, %i) \n", cA, rA, cB, rB);

        int x = rA * COLLUMNS + cA + 1;
        int y = rB * COLLUMNS + cB + 1;

        if(!isBlack()){
            x = 33 - x;
            y = 33 - y;
        }

        ostringstream os;

        os << (x) << "-" << (y) << "\n";

        return os.str();

    }
    vector<Move>* multiJump(pair<int, int> start, vector<pair<int, int>> path, vector<Move>* moves){

        if(path.size() == 0){
            path.push_back(start);
        }

        pair<int, int> left = getForwardLeft(start);
        pair<int, int> farLeft = getForwardLeft(getForwardLeft(start));

        pair<int, int> right = getForwardRight(start);
        pair<int, int> farRight = getForwardRight(getForwardRight(start));

        if(getField(left) == 3 && getField(farLeft) == 0){
            path.push_back(farLeft);
            moves->push_back(Move(path));
            multiJump(farLeft, path, moves);
        }
        if(getField(right) == 3 && getField(farRight) == 0){
            path.push_back(farRight);
            moves->push_back(Move(path));
            multiJump(farRight, path, moves);
        }
        return moves;
    };


    vector<Move> getMoves(pair<int, int> position){

        int unit = getField(position);

        vector<Move> moves;

        //printf("an pos (%i, %i): %i | links: (%i, %i): %i, rechts: (%i, %i): %i  \n", position.first, position.second, unit, getForwardLeft(position).first, getForwardLeft(position).second, getField(getForwardLeft(position)),getForwardRight(position).first, getForwardRight(position).second, getField(getForwardRight(position)));

        if(unit == 1){
            pair<int, int> left = getForwardLeft(position);
            pair<int, int> farLeft = getForwardLeft(getForwardLeft(position));

            pair<int, int> right = getForwardRight(position);
            pair<int, int> farRight = getForwardRight(getForwardRight(position));

            vector<pair<int, int>> steps;

            if(getField(getForwardLeft(position)) == 0){
                moves.push_back(Move(vector<pair<int, int>> {position, left}));
                //printf("Adding Move: %s", moves.back().str(isBlack()).c_str());
            }else if(getField(left) == 3 && getField(farLeft) == 0){

                vector<Move>* m = multiJump(position, vector<pair<int, int>>(),new vector<Move>());

                for(unsigned s = 0; s < m->size(); s++){
                    printf("Multi: %s", m->at(s).str(isBlack()).c_str());
                }

                steps.push_back(position);

                moves.push_back(Move(vector<pair<int, int>> {position, farLeft}));
            }

            if(getField(getForwardRight(position)) == 0){
                moves.push_back(Move(vector<pair<int, int>> {position, right}));
                //printf("Adding Move: %s", moves.back().str(isBlack()).c_str());
            }else if(getField(right) == 3 && getField(farRight) == 0){

                vector<Move>* m = multiJump(position, vector<pair<int, int>>(),new vector<Move>());

                for(unsigned s = 0; s < m->size(); s++){
                    printf("Multi (%i): %s", m->size(), m->at(s).str(isBlack()).c_str());
                    moves.push_back(m->at(s));
                }

                //moves.push_back(Move(vector<pair<int, int>> {position, farRight}));
            }


        }
        else if(unit == 2){

        }
        return moves;
    }

    void draw(){

        printf("\n\n");

        int x, y;

        for(int row = 0; row < 8; row++){
            for(int collumn = 0; collumn < 8; collumn++) {

                if((row + collumn) % 2 == 1){
                    x = (int) (collumn / 2.0);
                    y = 7 - row;

                    //printf("%i  ", canMove(collumn, row));
                    printf("%i ", field[x][y]);

                }else{
                    printf("-  ");
                }
            }
            printf("\n-------------------\n");
        }
    }
    vector<pair<int, int>> getPositions() {

        vector<pair<int, int>> positions;

        for (int row = 0; row < ROWS; row++) {
            for (int collumn = 0; collumn < COLLUMNS; collumn++) {

                if(getField(collumn, row) == 1){
                    positions.push_back(pair<int, int>(collumn, row));
                    //printf("Position: (%i, %i) \n", collumn, row);
                }

            }
        }
        return positions;
    }
    vector<Move> getAllMoves(vector<pair<int, int>> positions){

        vector <Move> moves;

        for(unsigned i = 0; i < positions.size(); i++){

            vector<Move> move = getMoves(positions.at(i));

            moves.insert(moves.end(), move.begin(), move.end());

        }

        for(unsigned k = 0; k < moves.size(); k++){
            //printf("Move: %s \n", moves.at(k).str(isBlack()).c_str());
        }
        return moves;
    }
    float evaluateMove(Move move){

        float value = 0;


        value += move.numberOfJumps(isBlack());

        printf("value: %f", value);

        return value;
    }

    string nextMove(){

        setupField();

        draw();

        //printf("test \n");

        vector<Move> moves = getAllMoves(getPositions());

        float bestMove = -1;
        int index = 0;



        for(unsigned i = 0; i < moves.size(); i++){

            float score = evaluateMove(moves.at(i));

            printf("aaahhh %s", moves.at(i).str(isBlack()).c_str());

            if(score > bestMove){
                bestMove = score;
                index = i;
            }
        }
        return moves.at(index).str(isBlack());
    }
};


#endif //CHECKAZ_CHECKERS_H
